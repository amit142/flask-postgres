from flask import Flask
from datetime import datetime
from sqlalchemy import Column, Integer, String, DateTime
from flask_sqlalchemy import SQLAlchemy

from server.config import settings

app = Flask(__name__,  static_folder='../../web/static', template_folder='../../web/templates')
app.config["SQLALCHEMY_DATABASE_URI"] = settings.SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = settings.SQLALCHEMY_TRACK_MODIFICATIONS
    
db = SQLAlchemy(app)

class Todo(db.Model):
    id = Column(Integer, primary_key=True)
    content = Column(String(200), nullable=False)
    date_created = Column(DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<Task %r>' % self.id